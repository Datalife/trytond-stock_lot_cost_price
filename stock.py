# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.modules.product import price_digits


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    cost_price = fields.Function(fields.Numeric(
        'Cost Price', digits=price_digits), 'get_cost_price')

    def get_cost_price(self, name=None, date=None):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')

        if not date:
            date = Date.today()
        moves = Move.search([
            ('lot', '=', self),
            ('state', 'in', ['assigned', 'done']),
            ('origin', 'like', 'purchase.line,%'),
            ('effective_date', '<=', date)
        ], order=[('effective_date', 'DESC')])
        if moves:
            return moves[0].origin.unit_price


class LotsByLocations(metaclass=PoolMeta):
    __name__ = 'stock.lots_by_locations'

    cost_price = fields.Function(fields.Numeric(
        'Cost price', digits=price_digits), 'get_lot')
